# IAM Access and Secret Key for your IAM user

# Name of the key pair in AWS, MUST be in same region as EC2 instance
# Check README for AWS CLI commands to create a key pair
key_name = "devops-practice"

# Local path to pem file for key pair. 
# Windows paths need to use double-backslash: Ex. C:\\Users\\Ned\\Pluralsight.pem
private_key_path = "D:\\doc\\nagarro doc\\keys\\devops-practice.pem"
