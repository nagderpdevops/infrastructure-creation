# OBJECTIVE

The goal is to automate the infrastructure creation so that new instances can be spawned on demand.This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository contains?

- Files to configure consul as backend(localhost)
- Terraform files for building infrastructure(AWS)
- Ansible playbook to configure the infrastructure

### Pre-requisite softwares

* Consul installed and configured
* Terraform installed
* AWS credentials configured for terraform
* Docker installed
* Ansible slave container pulled 'nabla/ansible-jenkins-slave-docker'
* Jenkins running as a docker container

### How do I get set up?

* Clone this repository
* Go to consul directory
* Create data directory if not exists
* Launch consul server instance using `consul agent -bootstrap -config-file="config/consul-config.hcl" -bind="127.0.0.1"`
* get token `consul acl bootstrap`, this will give a SecretID
* add this to env using
* Linux and MacOS user `export CONSUL_HTTP_TOKEN=SECRETID_VALUE`
* Windows user `$env:CONSUL_HTTP_TOKEN="SECRETID_VALUE"`
* Now configure Consul using Terraform and set up paths, policies, and tokens (from consul directory)

  ```
  terraform init
  terraform plan -out consul.tfplan
  terraform apply consul.tfplan
  ```
* Get token values for applications and record them for later
  `consul acl token read -id applications`
* Replace this token with env CONSUL_HTTP_TOKEN using above method
* In docker,
  * Enable "Expose daemon on tcp://localhost:2375 without TLS" from settings
  * Configure docker api with jenkins to swap container on demand as jenkins agents

### How to configure?

* In jenkins, install these plugins.
  * Git
  * Docker
  * Terraform
  * Ansible
* Configure terraform with install automatically version 1.0.4 linux64.
* Configure ansible and set "Path to ansible executables directory" to `/usr/local/bin/`
* Credentials to add to jenkins for pipeline to work
  * Bitbucket creds as username and password
  * AWS access key  and secret access key as secret text with ID aws_access_key and ID aws_secret_access_key
  * SSH pem key as SSH Username with private key (used by ansible to connect to ec2)
  * Applications consul token as a secret text with ID applications_consul_token
  * DockerHub creds as username and password
* In jenkins, change "Number of executors" in master node to '0'.
* And attach a slave container from docker image
  * Add docker agent template for Docker image `nabla/ansible-jenkins-slave-docker` and attach docker hub credentials for registory authentications
  * Set "Remote File System Root" to `/home/jenkins`

### How to run?
* Create a job of pipeline type
* Use this [Jenkinsfile](JenkinsFile) to configure pipeline
* Change credendials id in the pipeline script as required.
* Run the job


